#!/usr/bin/php
<?PHP

function data()
{
    for ($i = 0; $i < 10; ++$i) {
        yield "key_$i" => "hello_$i";
    }
}

foreach (data() as $k => $v) {
    printf("%s => %s\n", $k, $v);
}

