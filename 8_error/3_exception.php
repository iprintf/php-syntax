#!/usr/bin/php
<?PHP

class myException extends Exception
{
    public function __construct($msg, $code)
    {
        parent::__construct($msg, $code);
    }

    public function __toString()
    {
        return "自定义异常类!\n";
    }
}

set_exception_handler(function (Exception $e){
    if ($e instanceof myException) {
        echo $e, PHP_EOL;
        echo $e->getMessage(), PHP_EOL;
    } else {
        printf("标准异常信息: %s, 异常码: %d\n", $e->getMessage(), $e->getCode());
    }
});

echo "正常代码...!", PHP_EOL;
$i = 6;
$y = 10;
if ($i == 5)
  throw new Exception("PHP标准异常类!", 155);
if ($y = 10)
  throw new myException("Y等于10的异常!", 5667);


/*
 * try {
 *     echo "正常代码...!", PHP_EOL;
 *     $i = 6;
 *     $y = 10;
 *     if ($i == 5)
 *         throw new Exception("PHP标准异常类!", 155);
 *     if ($y = 10)
 *         throw new myException("Y等于10的异常!", 5667);
 *
 * } catch (myException $e) {
 *     echo $e, PHP_EOL;
 *     echo $e->getMessage(), PHP_EOL;
 * } catch (Exception $e) {
 *     printf("标准异常信息: %s, 异常码: %d\n", $e->getMessage(), $e->getCode());
 * } finally {
 *     echo "退出程序!", PHP_EOL;
 * }
 *
 */
