#!/usr/bin/php5
<?PHP

// ini_set("error_reporting", 0);
error_reporting(-1);

set_error_handler(function($no, $str, $file, $line){
    // printf("%s(%d): %s\n", $file, $line, $str);
    $s = "$file($line): $str\n";
    file_put_contents("./error.log", $s, FILE_APPEND);
    // exit(0);
});

// ereg("^hello", "hello world");

// $i = 10;
if (!isset($i))
    trigger_error("i 没有定义!\n", E_USER_ERROR);

// if (isset($i) && $i < 5)
if ($i < 5)
    echo "hello";

$b = 0;
// if ($b != 0)
    $a = 5 / $b;

// if (function_exists("fun"))
    fun(1);

// echo "world"

