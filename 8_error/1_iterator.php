#!/usr/bin/php
<?PHP

class kyo implements Iterator
{
    private $num = 0;
    private $data = null;

    public function __construct()
    {
        $this->data = array(
            "key1" => "hello1",
            "key2" => "hello2",
            "key3" => "hello3",
            "key4" => "hello4",
            "key5" => "hello5",
            "key6" => "hello6",
            "key7" => "hello7"
        );
        // $this->data = range(1, 10);
        reset($this->data);
    }

    public function current()
    {
        // printf("%s data = %s\n", __METHOD__, $this->data[$this->num]);
        // return $this->data[$this->num];
        return current($this->data);
    }

    public function next()
    {
        // printf("%s data = %s\n", __METHOD__, $this->data[$this->num]);
        // ++$this->num;
        next($this->data);
    }

    public function key()
    {
        // return $this->num;
        return key($this->data);
    }
    public function rewind()
    {
        echo "rewind\n";
        // $this->num = 0;
        reset($this->data);
    }
    public function valid()
    {
        // echo "valid\n";
        // return isset($this->data[$this->num]);
       return current($this->data) === FALSE ? FALSE : TRUE;
    }
}

foreach (new kyo() as $k => $v) {
    printf("%s => %s\n", $k, $v);
}

foreach (new DirectoryIterator("./") as $dir) {
    echo $dir->getFileName(), PHP_EOL;
}





