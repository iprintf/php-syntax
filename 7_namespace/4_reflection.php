#!/usr/bin/php
<?PHP

class kyo
{
    private $x = 100;
    public $y = 100;
    protected $z = 100;

    public function show($a, $b)
    {
        printf("kyo show a = %d, b = %d, x = %d, y = %d\n",
            $a, $b, $this->x, $this->y);
    }
}

function test()
{

}
// printf("test is %d\n", function_exists("test"));
// print_r(get_defined_functions());
$a = new kyo();

/*
 * $class = get_class($a);
 *
 * $methods = get_class_methods($class);
 * $vars = get_class_vars($class);
 *
 * print_r($methods);
 * print_r($vars);
 * $show = $methods[0];
 *
 * $b = new $class();
 * $b->$show(1, 2);
 */

/*
 * $ref = new ReflectionClass($a);
 * $pro = $ref->getProperties();
 *
 * // print_r($pro);
 *
 * $name = $pro[1]->name;
 * $class = $pro[1]->class;
 * $b = new $class;
 * echo $b->$name, PHP_EOL;
 *
 * $method = $ref->getMethods();
 *
 * print_r($method);
 * echo $method[0]->name, PHP_EOL;
 */

class mysql
{
    public function connect()
    {
        echo "mysql connect!!\n";
    }
}

class proxy
{
    private $target;

    public function __construct($target)
    {
        $this->target = $target;
    }

    public function __call($name, $args)
    {
        $obj = new $this->target;
        $ref = new ReflectionClass($obj);
        $method = $ref->getMethod($name);
        if ($method) {
            $method->invoke($obj, $args);
        }
    }
}


$sql = new proxy('mysql');
$sql->connect();






