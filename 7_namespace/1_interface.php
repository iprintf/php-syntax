#!/usr/bin/php
<?PHP

interface ibase
{
    const N = 10;

    function show();
}

class base
{
    private $a = 10;

    public function __get($name)
    {
        return $this->$name;
    }
}


class kyo extends base implements ibase
{
    public function show()
    {
        echo ibase::N;
        echo " kyo show!\n";
    }
}

$v = new kyo();
$v->show();

