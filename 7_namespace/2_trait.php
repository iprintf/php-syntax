#!/usr/bin/php
<?PHP

trait common
{
    private $ta = 100;
    public $tb = "common";

    public function show()
    {
        printf("trait common ta = %d, tb = %s, x = %d\n",
                $this->ta, $this->tb, $this->x);
    }
}

class kyo
{
    private $x = 1999;
    use common;
}

class base
{
    private $x = 8888;
    use common;

    public function test()
    {
        printf("ta = %d\n", $this->ta);
    }
}

$v = new kyo();
$v->show();

$v = new base();
$v->show();
$v->test();

