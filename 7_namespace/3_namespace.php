#!/usr/bin/php
<?PHP

namespace kyo {

    $data = 100;

    class base { }

    function show()
    {
        echo __NAMESPACE__." show!\n";
    }

}

namespace sub {

    $data = 100;

    class base { }

    function show()
    {
        echo __NAMESPACE__." show!\n";
    }

    use function kyo\show as kshow;
    use kyo as k;

    \kyo\show();
    k\show();
    kshow();
    show();
}


