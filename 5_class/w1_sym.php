#!/usr/bin/php
<?php

function add(int $a, int $b) : int
{
    return $a + $b;
}

function sub(int $a, int $b) : int
{
    return $a - $b;
}

function mul(int $a, int $b) : int
{
    return $a * $b;
}

function div(int $a, int $b) : int
{
    return $a / $b;
}

function mod(int $a, int $b) : int
{
    return $a % $b;
}

function power(int $a, int $b) : int
{
    $t = $a;

    while (--$b)
    {
        $t *= $a;
    }

    return $t;
}

function main()
{
    // $ch = array('+', '-', '*', '/', '%', '^');
    // $op = array("add", "sub", "mul", "div", "mod", "power");
    $op = array(
        '+' => 'add',
        '-' => 'sub',
        '*' => 'mul',
        '/' => 'div',
        '%' => 'mod',
        '^' => 'power'
    );

    foreach ($op as $i_ch => $i_f) {
        foreach ($op as $j_ch => $j_f) {
            if ($j_f($i_f(5, 3), 2) == 4)
                printf("(5 %s 3) %s 2 =4\n", $i_ch, $j_ch);
        }
    }

    /*
     * for ($i = 0; $i < 6; ++$i) {
     *     for ($j = 0; $j < 6; ++$j) {
     *         if ($op[$j]($op[$i](5, 3), 2) == 4)
     *             printf("(5 %s 3) %s 2 = 4\n", $ch[$i], $ch[$j]);
     *     }
     * }
     */

    return 0;
}

exit(main());

