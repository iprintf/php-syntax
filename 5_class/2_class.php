#!/usr/bin/php
<?PHP

function test($log)
{
    $log->show();
}

test(new class {
    public function show()
    {
        echo "test show!\n";
    }
});

class base {}
class kyo extends base {}

$obj = new kyo();

if ($obj instanceof kyo)
    echo "obj是kyo类的对象!\n";
if ($obj instanceof base)
    echo "obj是base类的子类的对象!\n";

