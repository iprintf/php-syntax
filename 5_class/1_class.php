#!/usr/bin/php
<?PHP

class kyo
{
    const MAX = 1024;
    public $b = 456;
    private $a = 123;

    public function __construct($id = 0, $name = "kyo")
    {
        echo "kyo init!!\n";
        $this->a = $id;
        $this->b = $name;
    }

    public function __destruct()
    {
        echo "delete kyo\n";
    }

    public function show()
    {
        echo "kyo MAX = ", kyo::MAX, PHP_EOL;
        echo "kyo a = ", $this->a, PHP_EOL;
        echo "kyo b = ", $this->b, PHP_EOL;
        echo "kyo show!\n";
    }

    public function getVal(object $obj, array $arr)
    {

    }
}

// $a = new kyo;
$a = new kyo(12, "KYO");
// $classname = "kyo";
// $a = new $classname();
//
// echo $a->a, PHP_EOL;
// echo kyo::MAX, PHP_EOL;
// echo $a->b, PHP_EOL;

$a->show();


