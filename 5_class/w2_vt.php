#!/usr/bin/php
<?PHP

const VT_DIR_UP = 'A';
const VT_DIR_DOWN = 'B';
const VT_DIR_LEFT = 'D';
const VT_DIR_RIGHT = 'C';

const VT_CUR_SAVE = 's';
const VT_CUR_LOAD = 'u';
const VT_CUR_HIDE = '?25l';
const VT_CUR_SHOW = '?25h';

const VT_COLOR_NONE = 10;
const VT_COLOR_BLACK = 30;
const VT_COLOR_RED = 31;
const VT_COLOR_GREEN = 32;
const VT_COLOR_YELLOW = 33;
const VT_COLOR_BLUE = 34;
const VT_COLOR_PURPLE = 35;
const VT_COLOR_WATHET = 36;
const VT_COLOR_WHITE = 37;

const VT_CLEAN_SCREEN = "2J";
const VT_CLEAN_LINEALL = "2K";
const VT_CLEAN_LINE = "K";

class vt
{
    public function gotoxy(int $r = 1, int $c = 1)
    {
        printf("\033[%d;%dH", $r, $c);
    }

    public function move(int $step, $dir = VT_DIR_DOWN)
    {
        printf("\033[%d%s", $step, $dir);
    }

    public function cursor($op = VT_CUR_HIDE)
    {
        printf("\033[%s", $op);
    }

    public function output($content, $blod = FALSE, $fcolor = VT_COLOR_NONE, $bcolor = VT_COLOR_NONE)
    {
        printf("\033[%d;%d;%dm$content\033[0m", $blod, $fcolor, $bcolor + 10);
    }

    public function clean($op = VT_CLEAN_SCREEN)
    {
        printf("\033[%s", $op);
    }
}

/*
 * $vt = new vt();
 * 
 * $vt->clean();
 * $vt->gotoxy();
 * $vt->output("hello", FALSE, VT_COLOR_RED, VT_COLOR_BLACK);
 * 
 * fgets(STDIN);
 */

