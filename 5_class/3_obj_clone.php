#!/usr/bin/php
<?PHP

class base
{
    public $baseval = 0;

    public function show()
    {
        echo "base val = ", $this->baseval, PHP_EOL;
    }
}

class kyo
{
    public $val;
    public $id = 0;
    public $name = "";

    public function __construct($val, $id = 0, $name = "")
    {
        $this->val = $val;
        $this->id = $id;
        $this->name = $name;
        echo "init!\n";
    }

    public function __clone()
    {
        $this->val = clone $this->val;
        echo "clone!!!!!!\n";
    }

    public function show()
    {
        $this->val->show();
        echo"id = ", $this->id, ", name = ", $this->name, PHP_EOL;
    }
}

$v = new base();
$v->baseval = 199;

$a = new kyo($v, 11, "hello");

$a_str = serialize($a);

echo "a_str = ", $a_str, PHP_EOL;

$c = unserialize($a_str);
$c->val->baseval = 8888;

// $b = $a;
$b = clone $a;

$b->id = 99;
$b->val->baseval = 1000;

$a->show();
$b->show();
$c->show();

print_r($a);

