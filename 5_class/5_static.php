#!/usr/bin/php
<?PHP

class kyo
{
    private $id;
    public static $count = 99;

    public function __construct($id = 0)
    {
        $this->id = $id;
        self::$count++;
    }

    public function show()
    {
        printf(__CLASS__." id = %d, count = %d\n", $this->id, static::$count);
    }

    static public function hello()
    {
        echo self::$count, PHP_EOL;
    }

    public function __call($name, $arg)
    {
        echo "__call: $name\n";
        print_r($arg);
    }

    static public function __callStatic($name, $arg)
    {
        echo "__callStatic: $name\n";
        print_r($arg);
    }

    public function __get($name)
    {
        echo "__get name = $name\n";
    }

    public function __set($name, $val)
    {
        echo "__set name = $name, val = $val\n";
    }

    public function __isset($name)
    {
        echo "__isset name = $name\n";
    }

    public function __unset($name)
    {
        echo "__unset name = $name\n";
    }

    public function __sleep()
    {
        echo "__sleep serialize\n";
        return array();
    }

    public function __wakeup()
    {
        echo "__wakeup unserialize\n";
    }

    public function __toString()
    {
        return "123456!";
    }

    public function __invoke()
    {
        echo "__invoke!!\n";
    }

    static public function __set_state(array $p)
    {
        echo "__set_state!\n";
        return (new class {
            private $name;
        });
    }

}

echo "kyo::count = ", kyo::$count, PHP_EOL;
kyo::$count = 0;

$a = new kyo;
echo $a::$count, "\n";
$a::$count++;

$b = new kyo;

$a->show();
$b->show();

kyo::hello();



//__call
$a->www(1, 2, 3);
//__callStatic
$a::www(1, 2, 3);
//__get
echo $a->name;
//__set
$a->name = "hello";
//__isset
isset($a->name);
empty($a->name);
//__unset
unset($a->name);
//__sleep
$s = serialize($a);
//__wakeup
$c = unserialize($s);
//__toString
echo $c, PHP_EOL;

//__invoke
$c();

//__set_state
var_export($c);



