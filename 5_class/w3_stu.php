#!/usr/bin/php
<?PHP

require_once  __DIR__."/4_class_graph/menu.php";

const SEX = array("女", "男");
const TYPENAME = array('name', 'sex', 'age', 'id', 'en', 'cn');

class stu
{
    private $stu = array();
    private $m = null;
    private $sm = null;
    private $fm = null;
    public $find_list = null;
    public $find_name = null;
    private $last_id = 1;

    public function __construct()
    {
        $this->m = new menu(array(
            'show' => array("stu", "show"),
            'handle' => array("stu", "menu"),
            'data' => array($this),
            'num' => 8
        ));
        $this->sm = new menu(array(
            'show' => array("stu", "subshow"),
            'handle' => array("stu", "submenu"),
            'data' => array($this),
            'num' => 7
        ));
        $this->fm = new menu(array(
            'title' => "\t\t --- 查找操作 ---",
            'prompt' => "\t\t请输入",
            'quitmes' => "\t\t3. 退出"
        ));
        $this->fm->addItem("\t\t1. 编辑", array("stu", "find_edit"), $this);
        $this->fm->addItem("\t\t2. 删除", array("stu", "find_del"), $this);
    }

    static public function find_edit($input, $stu)
    {
        $new_data = $stu->getString("", "\t\t请输入新数据: ");

        foreach ($stu->find_list as $s) {
            $stu->edit($s['index'], $stu->find_name, $new_data);
        }

    }
    public function edit($index, $name, $val)
    {
        $this->stu[$index][$name] = $val;
    }

    static public function find_del($input, $stu)
    {
        foreach($stu->find_list as $s) {
            $stu->del($s['index']);
        }
    }

    public function del($index)
    {
        unset($this->stu[$index]);
    }

    static public function show($stu)
    {
        $num = $stu->num();

        echo "========>   学生成绩管理系统   <========\n";
        echo "========>  1. 添加             <========\n";
        echo "========>  2. 查询             <========\n";
        echo "========>  3. 遍历($num)          <========\n";
        echo "========>  4. 存储             <========\n";
        echo "========>  5. 加载             <========\n";
        echo "========>  6. 退出             <========\n";
        echo "请输入 [ 1 - 6 ] : ";
    }

    static public function menu($input, $stu)
    {
        switch ($input) {
            case 1:
                $stu->add();
                break;
            case 2:
                $stu->sm->show();
                break;
            case 3:
                $stu->travel();
                break;
            case 4:
                $stu->save();
                break;
            case 5:
                $stu->load();
                break;
            case 6:
                return TRUE;
        }

        return FALSE;
    }

    static public function subshow($stu)
    {
        echo "\t-----> 1. name  <-----\n";
        echo "\t-----> 2. sex   <-----\n";
        echo "\t-----> 3. age   <-----\n";
        echo "\t-----> 4. id    <-----\n";
        echo "\t-----> 5. en    <-----\n";
        echo "\t-----> 6. cn    <-----\n";
        echo "\t-----> 7. exit  <-----\n";
        echo "\t请输入查询类型 [ 1 - 7 ]: ";
    }

    public function getkey()
    {
        while (TRUE) {
            printf("\t请输入查询关键词: ");
            $key = rtrim(fgets(STDIN));
            if (strlen($key) == 0)
                continue;
            break;
        }

        return $key;
    }

    public function find($key, $type)
    {
        $find_list = array();

        foreach ($this->stu as $k => $s)
        {
            if ($key == $s[$type])
            {
                $s['index'] = $k;
                $find_list[] = $s;
            }

        }
        return $find_list;
    }

    static public function submenu($input, $stu)
    {
        if ($input == 7)
            return TRUE;

        $stu->find_name = TYPENAME[$input - 1];
        $stu->find_list = $stu->find($stu->getkey(), $stu->find_name);

        printf("    查找结果:\n");
        foreach ($stu->find_list as $s)
        {
            printf("    学号: %d, 姓名: %s, 性别: %s,".
                   " 年龄: %d, 语文: %d, 英文: %d\n",
                   $s['id'], $s['name'], SEX[$s['sex']],
                   $s['age'], $s['cn'], $s['en']);
        }

        $stu->fm->show();

        return FALSE;
    }

    public function num()
    {
        return count($this->stu);
    }

    public function add()
    {
        while (TRUE)
        {
            printf("请输入学生姓名: ");
            $name = rtrim(fgets(STDIN));
            $len = strlen($name);
            if ($len < 2 || $len > 20) {
                echo "输入的姓名不合法!\n";
                continue;
            }
            break;
        }
        $id = $this->last_id++;
        $sex = mt_rand(0, 1);
        $age = mt_rand(7, 26);
        $en = mt_rand(50, 100);
        $cn = mt_rand(50, 100);

        $this->stu[] = compact("id", "name", "sex", "age", "en", "cn");
    }

    public function travel()
    {
        printf("    遍历结果:\n");
        foreach ($this->stu as $s)
        {
            printf("    学号: %d, 姓名: %s, 性别: %s,".
                   " 年龄: %d, 语文: %d, 英文: %d\n",
                   $s['id'], $s['name'], SEX[$s['sex']],
                   $s['age'], $s['cn'], $s['en']);
        }
    }

    static public function getString($default_val = "", $pormpt = "请输入: ")
    {
        printf($pormpt);
        $path = rtrim(fgets(STDIN));
        if (strlen($path) == 0)
            $path = $default_val;

        return $path;
    }

    public function save()
    {
        $path = $this->getString("./db", "    保存路径(./db): ");

        $content = array(
            'last_id' => $this->last_id,
            'stu' => $this->stu
        );

        $ret = file_put_contents($path, serialize($content));
        if ($ret === FALSE)
            echo "保存失败!\n";
        else
            echo "保存成功!\n";
    }

    public function load()
    {
        $path = $this->getString("./db", "    加载路径(./db): ");

        $content = unserialize(file_get_contents($path));
        $this->stu = $content['stu'];
        $this->last_id = $content['last_id'];
    }

    public function run()
    {
        $this->m->show();
    }
}


$s = new stu();

$s->run();


