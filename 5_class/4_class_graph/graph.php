<?PHP

class graph
{
    private $top;
    private $border;
    private $dir;
    private $name;

    public function __construct($name = "", $border = "",
                                    $top = "", $dir = "")
    {
        if ($name == "")
            $name = "trapezoid";

        if ($border == "")
            $border = BORDER;

        if ($top == "")
            $top = TOP;

        if ($dir == "")
            $dir = L_UP;

        $this->name = $name;
        $this->top = $top;
        $this->border = $border;
        $this->dir = $dir;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function show($name = "")
    {
        if ($name == "")
            $name = $this->name;

        $this->$name();
    }

    public function getBorder()
    {
        return $this->border;
    }

    public function getTop()
    {
        return $this->top;
    }

    public function getDir()
    {
        return $this->dir;
    }


    public function setBorder($border)
    {
        $this->border = $border;
    }

    public function setHeight($height)
    {
        $this->border = $height;
    }

    public function setTrapezoid($top = 0, $height = 0)
    {
        if ($top != 0)
            $this->top = $top;
        if ($height != 0)
            $this->border = $height;
    }

    public function setTriangle($border = 0, $dir = L_UP)
    {
        if ($dir != 0)
            $this->dir = $dir;
        if ($border != 0)
            $this->border = $border;
    }

    public function setTop($top)
    {
        $this->top = $top;
    }

    public function setDir($dir)
    {
        $this->dir = $dir;
    }

    public function setVal($name, $value)
    {
        $this->$name = $value;
    }

    function trapezoid($top = 0, $height = 0)
    {
        if ($top === 0)
            $top = $this->top;
        if ($height === 0)
            $height = $this->border;

        for ($i = 0; $i < $height; ++$i) {
            for ($j = 0, $num = $height - $i - 1; $j < $num; ++$j) {
                echo "   ";
            }
            for ($j = 0, $num = $top + $i * 2; $j < $num; ++$j) {
                echo " * ";
            }
            echo "\n";
        }
    }

    function square($border = 0)
    {
        if ($border === 0)
            $border = $this->border;

        for ($i = 0; $i < $border; ++$i) {
            for ($j = 0; $j < $border; ++$j) {
                echo " * ";
            }
            echo "\n";
        }
    }

    function triangle($border = 0, $dir = 0)
    {
        if ($border === 0)
            $border = $this->border;
        if ($dir === 0)
            $dir = $this->dir;

        for ($i = 0, $m = $border * $border; $i < $m; ++$i) {
            $r = intdiv($i, $border);
            $c = $i % $border;

            if ($dir == L_UP && $c < $border - $r
                || $dir == L_DOWN && $c <= $r
                || $dir == R_UP && $c >= $r
                || $dir == R_DOWN && $c >= $border - $r - 1)
                echo " * ";
            else
                echo "   ";

            if ($c == $border - 1)
                echo "\n";
        }
    }
}

// $g = new graph("triangle", 7, "", L_DOWN);

// $g->setHeight(10);
// $g->setTrapezoid(5, 10);
// $g->trapezoid();
// $g->show("Square");
// $g->show();


