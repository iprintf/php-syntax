#!/usr/bin/php
<?PHP

require_once __DIR__."/common.php";
require_once __DIR__."/menu.php";
require_once __DIR__."/graph.php";


class app
{
    private $g = null;
    private $m = null;
    private $sm = null;

    public function __construct()
    {
        $this->g = new graph();
        $this->m = new menu(array(
            'show' => array("app", "showGraphMenu"),
            'handle' => array("app", "graphmenu"),
            'data' => array($this),
            'num' => 6
        ));

        $this->sm = new menu(array(
            'show' => array("app", "showTriangleSubMenu"),
            'handle' => array("app", "triangleMenu"),
            'data' => array($this),
            'num' => 6
        ));
    }

    static public function showGraphMenu($app)
    {
        $border = $app->g->getBorder();
        $top = $app->g->getTop();
        echo "=== 图形打印程序主菜单  ===", PHP_EOL;
        echo "=== 1. 梯形             ===", PHP_EOL;
        echo "=== 2. 正方形           ===", PHP_EOL;
        echo "=== 3. 直角三角形       ===", PHP_EOL;
        echo "=== 4. 设置边长/梯高($border) ===", PHP_EOL;
        echo "=== 5. 设置梯顶($top)      ===", PHP_EOL;
        echo "=== 6. 退出             ===", PHP_EOL;
        echo "请输入[1 - 6]: ";
    }

    static public function graphmenu($input, $app)
    {
        switch ($input) {
            case 1:
                $app->g->trapezoid();
                break;
            case 2:
                $app->g->square();
                break;
            case 3:
                $app->sm->show();
                break;
            case 4:
                $app->g->setBorder($app->getBorder());
                break;
            case 5:
                $app->g->setTop($app->getinput(3, 10, "\t请输入梯顶"));
                break;
            case 6:
                return TRUE;
        }

        return FALSE;
    }

    public function showTriangleSubMenu($app)
    {
        $border = $app->g->getBorder();
        echo "\t--- 直角三角形子菜单    ---", PHP_EOL;
        echo "\t--- 1. 左上边           ---", PHP_EOL;
        echo "\t--- 2. 左下边           ---", PHP_EOL;
        echo "\t--- 3. 右下边           ---", PHP_EOL;
        echo "\t--- 4. 右上边           ---", PHP_EOL;
        echo "\t--- 5. 设置边长 ($border)     ---", PHP_EOL;
        echo "\t--- 6. 返回上一级       ---", PHP_EOL;
        echo "\t--- 7. 退出             ---", PHP_EOL;
        echo "\t请输入[1 - 7]: ";
    }

    public function triangleMenu($input, $app)
    {
        switch ($input) {
            case L_UP:
            case L_DOWN:
            case R_UP:
            case R_DOWN:
                $app->g->setDir($input);
                $app->g->triangle();
                break;
            case 5:
                $app->g->setBorder($app->getBorder("\t\t请输入边长"));
                break;
            case 7:
                $app->m->menuQuit();
            case 6:
                return TRUE;
                break;
        }

        return FALSE;
    }

    function getInput($start = 3, $end = 20, $prompt = PROMPT)
    {
        return menu::getInput($start, $end, function() use ($start, $end, $prompt) {
            echo "${prompt}[$start - $end]: ";
        });
    }

    function getBorder($prompt = PROMPT)
    {
        return $this->getInput(3, 20, $prompt);
    }

    public function run()
    {
        $this->m->show();
    }
}

$app = new app();

$app->run();

