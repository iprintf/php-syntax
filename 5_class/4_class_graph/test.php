#!/usr/bin/php
<?PHP

require_once(__DIR__.'/menu.php');


/*
 * function hello($n, array $arg = array())
 * {
 *     echo "$n => $arg[0]", PHP_EOL;
 * }
 * 
 * $m = new menu(array(
 *     'title' => "=== 图形菜单 ===",
 *     'quitmes' => "=== 5. 退出"
 * ));
 * 
 * $m->addItem("=== 1. 11111", "hello", array("1111"));
 * $m->addItem("=== 2. 22222", "hello", array("2222"));
 * $m->addItem("=== 3. 33333", "hello", array("3333"));
 * $m->addItem("=== 4. 44444", "hello", array("4444"));
 * 
 * $m->show();
 * 
 * exit(0);
 */
//=====================================================

function showGraphMenu($border, $top)
{
    echo "=== 图形打印程序主菜单  ===", PHP_EOL;
    echo "=== 1. 梯形             ===", PHP_EOL;
    echo "=== 2. 正方形           ===", PHP_EOL;
    echo "=== 3. 直角三角形       ===", PHP_EOL;
    echo "=== 4. 设置边长/梯高($border) ===", PHP_EOL;
    echo "=== 5. 设置梯顶($top)      ===", PHP_EOL;
    echo "=== 6. 退出             ===", PHP_EOL;
    echo "请输入[1 - 6]: ";
}

function graphmenu($n, $border, $top)
{
    switch ($n) {
        case 1:
            echo "梯形!\n";
            break;
        case 2:
            echo "正方形!\n";
            break;
        case 3:
            echo "直角三角形!\n";
            break;
        case 6:
            return TRUE;
    }

    return FALSE;
}

$border = 5;
$top = 3;

$m = new menu(array(
    'show' => "showGraphMenu",
    'handle' => "graphmenu",
    'data' => array($border, $top),
    'num' => 6
));

$m->show();

