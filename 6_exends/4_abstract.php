#!/usr/bin/php
<?PHP

abstract class base
{
   protected $a = 123;

   abstract protected function init($data);

   protected function show()
   {
        echo "base!\n";
   }
}

class kyo extends base
{
    protected function init($data)
    {

    }
}


// $v = new base();

