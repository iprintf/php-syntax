#!/usr/bin/php
<?PHP

class base
{
    private $a = 123;
    public $b = "base";
    protected $c = 12.54;

    public function show()
    {
        printf("base a = %d, b = %s, c = %.2f\n", $this->a, $this->b, $this->c);
    }
}

class kyo extends base
{
    private $x = 456;
    public $y = "kyo";
    protected $z = 354.667;

    public function output()
    {
        $this->c = 78.99;
        $this->show();
        printf("kyo x = %d, y = %s\n", $this->x, $this->y);
    }

    public function show()
    {
        parent::show();
        echo "hello\n";
    }
}

class sub extends kyo
{
    public function subshow()
    {
        $this->c = 19.999;
        $this->output();
    }
}

$v = new kyo();

$v->show();

$v->show();
$v->b = 100;
$v->output();


$s = new sub();

$s->subshow();


