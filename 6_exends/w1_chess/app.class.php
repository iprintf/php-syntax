<?PHP

function __autoload($name)
{
    include_once __DIR__."/$name.class.php";
}

class app extends chess
{
    private $sdl = null;
    private $spare = 0;
    private $ball_r = 0;
    private $board_x;
    private $board_y;
    private $board_w;
    private $board_h;
    const W = 640;
    const H = 480;

    static public function getSpare($row, $col)
    {
        $ws = (int)(app::W / ($row + 1));
        $hs = (int)(app::H / ($col + 1));

        return $ws > $hs ? $hs : $ws;
    }

    public function __construct($row = 11, $col = 11)
    {
        if ($row % 2 == 0)
            $row--;
        if ($col % 2 == 0)
            $col--;

        $this->spare = self::getSpare($row, $col);
        $this->ball_r = (int)($this->spare / 2) - 3;
        $this->board_w = ($row - 1) * $this->spare;
        $this->board_h = ($col - 1) * $this->spare;
        $this->board_x = floor((app::W - $this->board_w) / 2);
        $this->board_y = floor((app::H - $this->board_h) / 2);


        $this->sdl = new sdl(app::W, app::H, 0xB39C36);

        $this->sdl->keys(SDLK_LEFT, array('app', 'key_handle'), $this);
        $this->sdl->keys(SDLK_RIGHT, array('app', 'key_handle'), $this);
        $this->sdl->keys(SDLK_UP, array('app', 'key_handle'), $this);
        $this->sdl->keys(SDLK_DOWN, array('app', 'key_handle'), $this);
        $this->sdl->keys(SDLK_SPACE, array('app', 'key_handle'), $this);

        parent::__construct($row, $col);
    }

    public function win()
    {
        $this->sdl->quit();
    }

    public function key_handle($key, $status, & $data)
    {
        switch ($key)
        {
            case SDLK_LEFT:
                if ($data->c > 0)
                    $data->c--;
                break;
            case SDLK_RIGHT:
                if ($data->c < $data->col - 1)
                    $data->c++;
                break;
            case SDLK_UP:
                if ($data->r > 0)
                    $data->r--;
                break;
            case SDLK_DOWN:
                if ($data->r < $data->row - 1)
                    $data->r++;
                break;
            case SDLK_SPACE:
                if ($data->handle())
                    return;
                break;
            default:
                return;
        }

        $data->show();
    }

    public function draw_board()
    {
        $this->sdl->bg();

        $this->sdl->box($this->board_x - 3, $this->board_y - 3, $this->board_w + 6, $this->board_h + 6, 2, 0x0);
        $this->sdl->box($this->board_x, $this->board_y, $this->board_w, $this->board_h, 1, 0x0);

        for ($i = 1; $i < $this->row - 1; ++$i)
        {
            $this->sdl->hline($this->board_x, $this->board_y + $i * $this->spare, $this->board_w, 0x0);
        }

        for ($i = 1; $i < $this->col - 1; ++$i)
        {
            $this->sdl->vline($this->board_x + $i * $this->spare, $this->board_y, $this->board_h, 0x0);
        }

        $ps = 3;

        $this->sdl->circle(
                $this->board_x + $this->c * $this->spare,
                $this->board_y + $this->r * $this->spare,
                $ps, 0x0);

        $this->sdl->circle(
                $this->board_x + 2 * $this->spare,
                $this->board_y + 2 * $this->spare,
                $ps, 0x0);

        $this->sdl->circle(
                $this->board_x + 2 * $this->spare,
                $this->board_y + ($this->col - 2 - 1) * $this->spare,
                $ps, 0x0);

        $this->sdl->circle(
                $this->board_x + ($this->row - 2 - 1) * $this->spare,
                $this->board_y + 2 * $this->spare,
                $ps, 0x0);

        $this->sdl->circle(
                $this->board_x + ($this->row - 2 - 1) * $this->spare,
                $this->board_y + ($this->col - 2 - 1) * $this->spare,
                $ps, 0x0);

        /*
         * printf("r =%d, c = %d, x = %d, y = %d\n",
         *         $this->r, $this->c,
         *         $this->board_x + $this->r * $this->spare,
         *         $this->board_y + $this->r * $this->spare);
         */
    }

    public function draw_cur()
    {
        $len = (int)($this->spare / 5);
        $x = $this->board_x + $this->c * $this->spare;
        $y = $this->board_y + $this->r * $this->spare;

        $border = 1;

        $this->sdl->rect($x - $this->ball_r, $y - $this->ball_r, $border, $len, 0x0);
        $this->sdl->rect($x - $this->ball_r, $y - $this->ball_r, $len, $border, 0x0);

        $this->sdl->rect($x - $this->ball_r, $y + $this->ball_r - $len, $border, $len, 0x0);
        $this->sdl->rect($x - $this->ball_r, $y + $this->ball_r, $len, $border, 0x0);

        $this->sdl->rect($x + $this->ball_r, $y - $this->ball_r, $border, $len, 0x0);
        $this->sdl->rect($x + $this->ball_r - $len, $y - $this->ball_r, $len, $border, 0x0);

        $this->sdl->rect($x + $this->ball_r, $y + $this->ball_r - $len + $border, $border, $len, 0x0);
        $this->sdl->rect($x + $this->ball_r - $len, $y + $this->ball_r, $len, $border, 0x0);
    }

    public function show()
    {
        $this->draw_board();

        for ($i = 0; $i < $this->row; ++$i) {
            for ($j = 0; $j < $this->col; ++$j){

                if ($i == $this->r && $j == $this->c)
                    $this->draw_cur();

                if ($this->board[$i][$j] == '@') {
                    $this->sdl->circle(
                        $this->board_x + $j * $this->spare,
                        $this->board_y + $i * $this->spare,
                        $this->ball_r,
                        0xFFFFFF
                    );
                }
                else if ($this->board[$i][$j] == '#') {
                    $this->sdl->circle(
                        $this->board_x + $j * $this->spare,
                        $this->board_y + $i * $this->spare,
                        $this->ball_r,
                        0x0
                    );
                }
            }
        }

        $this->sdl->flip();
    }

    public function run()
    {
        $this->sdl->run();
    }
}

$v = new app(11, 11);
$v->run();
