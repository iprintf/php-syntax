#!/usr/bin/php
<?PHP

class base
{
    function show()
    {
        echo "BASE Show!!\n";
    }

    function test()
    {
        $this->show();
    }
}

class kyo extends base
{
    private $k = 100;

    function show()
    {
        echo $this->k, " KYO Show!!\n";
    }
}

$v = new kyo();

$v->test();

