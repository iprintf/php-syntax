#!/usr/bin/php
<?PHP

/*
 * r       只读
 * r+      可读可写
 * w       只写, 文件不存在会创建文件, 文件存在会清空文件
 * w+      在w功能基础上增加可读
 * a       追加只写, 文件不存在会创建文件
 * a+      在a功能基础上增加可读
 * b       代表操作二进制文件
 */

$fp = fopen("./testfile", "r+");
// echo gettype($fp);
if (!$fp) {
    die("fopen failed!\n");
}

/*
 * fgetc            按字符获取
 * fgets            按行读
 * fscanf/fprintf   格式化I/O操作
 * fread/fwrite/fputs   按字节I/O操作
 */
/*
 * echo fgetc($fp), PHP_EOL;
 * echo fgetc($fp), PHP_EOL;
 * echo fgetc($fp), PHP_EOL;
 * echo fgetc($fp), PHP_EOL;
 * echo fgetc($fp), PHP_EOL;
 * fwrite($fp, "KYO");
 */
// echo rtrim(fgets($fp)), PHP_EOL;
// fscanf($fp, "%f", $s);
// echo $s, PHP_EOL;
// echo fread($fp, 10), PHP_EOL;

// fprintf($fp, "hello %d wo%srld\n", 100, "kyo");
// fwrite($fp, "hello kyo!!");

echo ftell($fp), PHP_EOL;

/*
 * SEEK_SET        从开头位置偏移
 * SEEK_CUR        从当前位置偏移
 * SEEK_END        从结尾位置偏移
 */

fseek($fp, 10, SEEK_SET);
echo "fseek 10 = ", ftell($fp), PHP_EOL;
fseek($fp, 10, SEEK_CUR);
echo "fseek 10 = ", ftell($fp), PHP_EOL;
fseek($fp, 0, SEEK_END);
echo "fseek end = ", ftell($fp), PHP_EOL;
fseek($fp, -10, SEEK_END);
echo "fseek 10 = ", ftell($fp), PHP_EOL;


fclose($fp);



