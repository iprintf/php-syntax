#!/usr/bin/php5
<?PHP

function main()
{
    if (!extension_loaded("sdl")) {
       if (!dl("sdl.so"))
           die("sdl扩展加载失败!\n");
    }

    SDL_Init(SDL_INIT_VIDEO);

    $screen = SDL_SetVideoMode(640, 480, 32, SDL_SWSURFACE);

    $rect = array(
        'x' => 100,
        'y' => 100,
        'w' => 50,
        'h' => 100
    );
    SDL_FillRect($screen, $rect, SDL_MapRGB($screen['format'], 255, 0, 0));

    SDL_Flip($screen);


    while (TRUE)
    {
        if (SDL_WaitEvent($event))
        {
            if ($event['type'] == SDL_QUIT)
                break;
            else if ($event['type'] == SDL_KEYDOWN)
            {
                if ($event['key']['keysym']['sym'] == 27)
                    break;
            }
            else if ($event['type'] == SDL_MOUSEMOTION)
            {
                // printf("x = %d, y = %d\n", $event['motion']['x'], $event['motion']['y']);
            }
            else if ($event['type'] == SDL_MOUSEBUTTONDOWN)
            {
                if ($event['button']['button'] == SDL_BUTTON_LEFT)
                printf("x = %d, y = %d\n", $event['button']['x'], $event['button']['y']);
            }
        }
    }

    SDL_Quit();

    return 0;
}

exit(main());

