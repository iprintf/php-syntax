<?PHP

require_once __DIR__."/menubase.php";

class menu extends menuBase
{
    private $myshow = null;
    private $handle = null;
    private $my_data = null;
    private $quit = TRUE;
    private $itemNum = 0;

    public function __construct($arg = array())
    {
        parent::__construct($arg);

        if (isset($arg['show']))
            $this->myshow = $arg['show'];

        if (isset($arg['handle']))
            $this->handle = $arg['handle'];

        if (isset($arg['data']))
            $this->my_data = $arg['data'];

        if ($this->myshow != null)
        {
            if (isset($arg['num']))
                $this->itemNum = $arg['num'];
            else {
                echo "请指定菜单条目个数!\n";
                return null;
            }
        }
    }

    public function setMyMenu($show, $handle, ...$arg)
    {
        $this->myshow = $show;
        $this->handle = $handle;
        $this->my_data = $arg;
    }

    public function getMenuInput()
    {
        if ($this->myshow == null)
            return parent::getMenuInput();

        return self::getInput(1, $this->itemNum + 1, $this->myshow, ...$this->my_data) - 1;
    }

    public function run($n)
    {
        if ($this->handle == null)
            return parent::run($n);

        if (call_user_func($this->handle, $n + 1, ...$this->my_data))
            return TRUE;
        return FALSE;
    }
}
