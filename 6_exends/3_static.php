#!/usr/bin/php
<?PHP

final class A {}

class base
{
    private $a = 123;
    public $b = "base";
    static public $c = 12.45;

    public static function test()
    {
        echo "BASE test function!\n";
    }

    static public function start()
    {
        self::test();
        // static::test();
    }

    final public function init()
    {
        echo "base init!\n";
    }

    public function show()
    {
        echo "base a = $this->a, b = $this->b\n";
    }
}

class kyo extends base
{
    private $a = 456;
    public $b = "kyo";
    // static public $c = 88.88;

    public static function test()
    {
        echo "KYO test function!\n";
    }

    public function show()
    {
        // echo "static parent c = ", parent::$c, PHP_EOL;
        echo "static self c = ", self::$c, PHP_EOL;
        echo "static static c = ", static::$c, PHP_EOL;
        echo "kyo a = $this->a, b = $this->b\n";
    }
}

// kyo::start();
// base::start();

$v = new kyo();

$v->show();

