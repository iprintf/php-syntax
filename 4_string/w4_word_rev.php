#!/usr/bin/php
<?php

function mystrtok($s)
{
    $w[] = strtok($s, " \n");

    while (($word = strtok(" \n")) != FALSE)
        $w[] = $word;

    return $w;
}

function main()
{
    printf("请输入字符串: ");
    $s = fgets(STDIN);

    $w = mystrtok($s);

    for ($i = count($w) - 1; $i >= 0; --$i)
    {
        echo $w[$i], " ";
    }
    echo PHP_EOL;

    return 0;
}

exit(main());

