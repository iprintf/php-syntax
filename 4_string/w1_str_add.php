#!/usr/bin/php
<?PHP

function add($num, & $bit): string
{
    $bit = intdiv($num, 10);

    return (string)($num % 10);
}

function bitop($start, & $arr, $m): string
{
    for ($i = $start, $s = ""; $i >= 0; --$i)
    {
        $s .= add($m + $arr[$i], $m);
    }

    return $s;
}

function main()
{
    $a = "999999";
    $b = "1";

    printf("请输入两个数字: ");
    fscanf(STDIN, "%s%s", $a, $b);

    $al = strlen($a);
    $bl = strlen($b);
    $m = 0;
    $s = "";

    $t = $al > $bl ? $bl : $al;
    $ai = $al - 1;
    $bi = $bl - 1;

    while ($t--)
    {
        $s .= add($a[$ai--] + $b[$bi--] + $m, $m);
    }

    $s .= $al > $bl ? bitop($al - $bl - 1, $a, $m)
                    : bitop($bl - $al - 1, $b, $m);

    if ($m != 0)
        $s .= $m;

    printf("%s + %s = ", $a, $b);
    echo strrev($s), PHP_EOL;


    return 0;
}


exit(main());

