#!/usr/bin/php
<?php

function main()
{
    /*
     * printf("请输入字符串1: ");
     * $s1 = fgets(STDIN);
     * printf("请输入字符串2: ");
     * $s2 = fgets(STDIN);
     */
    $s1 = "howareyou";
    $s2 = "hwwarettwareyou";

    $l1 = strlen($s1);
    $l2 = strlen($s2);
    $max = 2;

    for ($i = 0; $i < $l1; ++$i) {
        $find_str = $s2;
        while (($index = strpos($find_str, $s1[$i])) !== FALSE)
        {
            $cmp_num = strlen($find_str) - $index;
            $cmp_s1 = substr($s1, $i);
            $cmp_s2 = substr($find_str, $index);

            // printf("cmp_num = %d, cmp_s1 = %s, cmp_s2 = %s, max = %d, fstr = ", $cmp_num, $cmp_s1, $cmp_s2, $max);

            for ($j = $max; $j <= $cmp_num; ++$j) {

                if (strncmp($cmp_s1, $cmp_s2, $j))
                    break;

                $common_str = substr($cmp_s1, 0, $j);
                $max = $j;
            }
            $find_str = substr($find_str, $index + 1);

            // echo $find_str, PHP_EOL;
        }
    }

    printf("s1 = %s, s2 = %s\n", $s1, $s2);
    printf("max = %d, common_str = %s\n", $max, $common_str);





    return 0;
}

exit(main());

