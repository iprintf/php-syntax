#!/usr/bin/php
<?PHP

CONST UA = 65;
CONST LA = 97;
CONST UZ = 92;
CONST LZ = 123;

function ws($s): bool
{
    $ch = ord($s);
    if (($ch >= UA && $ch <= UZ
            || $ch >= LA && $ch <= LZ))
        return FALSE;
    return TRUE;
}

function word_count(string $s): int
{
    // "      hello       world     "
    // "hello world"

    $l = strlen($s) - 1;

    for ($i = 0, $count = 0; $i < $l; ++$i) {
        if (!ws($s[$i]) && ws($s[$i + 1]))
            $count++;
    }

    if (!ws($s[$l]))
        $count++;

    return $count;
}

function main()
{
    printf("请输入字符串: ");
    $s = fgets(STDIN);

    printf("单词个数为: %d\n", word_count($s));

    return 0;
}

exit(main());

