#!/usr/bin/php
<?PHP

function test($val = 100)
{
    echo "test = ", $val, "\n";
}

function kyo($val = 100)
{
    echo "kyo = ", $val, "\n";
}

function sub($call, $val)
{
    $call($val);
}

$a = "test";

// $a(999);
sub($a, 999);

$a = "kyo";
sub($a, 1000);

// $a(1111);

$a = function($val = 888) {
    echo "closures function ", $val, "\n";
};

// sub($a, 666);
// $a();
sub(function($val = 1234){
    echo "argv function $val\n";
}, 6788);


$prefix = "KYO: ";
$dom = "baidu.com";

$my = function ($val = 100) use (& $prefix, $dom) {
    echo "$prefix, colosures $dom val = $val!\n";
    $prefix = "WWW";
};

$my(1888);

echo "prefix = $prefix\n";



