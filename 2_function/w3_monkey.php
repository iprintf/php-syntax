#!/usr/bin/php
<?PHP

function monkey($day)
{
    if ($day == 0)
        return 1;
    return (monkey($day - 1) + 1) * 2;
}

echo "没吃之前桃总数: ", monkey(5), PHP_EOL;
