#!/usr/bin/php
<?PHP

$run = 10;

function test(int & $a)
{
    // 如果声明run为全局变量 ，则在函数内部可以访问
    // global $run;
    echo "test a = ", $a, PHP_EOL;
    $a = 9999;
    //不能访问函数外部定义变量
    // echo "test run = ", $run, PHP_EOL;
    // $run = 177;
}

//默认值
function kyo(int $a = 10, int $b = 100, int $c = 1000)
{
    printf("kyo function a = %d, b = %d, c = %d\n", $a, $b, $c);
}

//可变参
// function change(...$argv)
// function change(int ...$argv)
function change(int & ...$argv)
{
    $argv[0] = 18888;
    echo "change argv list: ";
    // echo $argv[0], $argv[1], $argv[2], PHP_EOL;
    var_dump($argv);
}

function change_old()
{
    echo 'argv num = ', func_num_args(), PHP_EOL;
    echo "argv 3 val = ", func_get_arg(3), PHP_EOL;
    var_dump(func_get_args());
}

test($run);
kyo();
kyo(9);
kyo(9, 99);
kyo(9, 99, 999);
// $a = [8, 88, 888];
// kyo(...$a);
// kyo(...[9, 99, 999]);

// change(1, 2, "hello", 4, 234.67, 6, 7);
// change(1, 2, 3, 4, 5, 6, 7);
change($run);
change_old(1, 2, 3, 4, 5, 6, 7);

echo "run = ", $run, PHP_EOL;

