#!/usr/bin/php
<?PHP

// 函数开关变量
$kyo = false;

//开关变量为真才定义test函数
if ($kyo)
{
    function test(int $a, string $b, float $c): int
    {
        echo "a = ", $a, ", b = ", $b, ", c= ", $c, PHP_EOL;
        echo "hello world!\n";
        return 123;
    }
}

function kyo_test()
{
    echo "kyo_test start!\n";
    function kyo_sub()
    {
        echo "kyo_sub runing!\n";
    }
    // kyo_sub();
    echo "kyo_test end!\n";
}

$r = 0;
//开关变量为真才调用test函数
$kyo and $r = test(11, "222", 3677.334);
echo "test = ", $r, PHP_EOL;

kyo_test();

echo "is kyo_sub = ", function_exists("kyo_sub"), PHP_EOL;


kyo_sub();


