#!/usr/bin/php
<?PHP

//封装通用输入数判断某数的函数
function getIsNum($str, $call)
{
    while (TRUE)
    {
        echo "请输入一个整数: ";
        fscanf(STDIN, "%d", $num);

        if ($num == -1)
            break;

        if ($call($num))
            printf("%d 是%s!\n", $num, $str);
        else
            printf("%d 不是%s!\n", $num, $str);
    }
}

//判断某个数是否为素数
function isPrime(int $num) : bool
{
    if ($num <  3)
        return FALSE;

    for ($i = (int)($num / 2); $i >= 2; --$i)
    {
        if ($num % $i == 0)
            return FALSE;
    }

    return TRUE;
}

//求两个数的最大公约数
function getCommonNum(int $num1, int $num2): int
{
    $num1 = abs($num1);
    $num2 = abs($num2);

    for ($min = $num1 > $num2 ? $num2: $num1;  $min > 0; --$min)
    {
        if ($num1 % $min == 0 && $num2 % $min == 0)
            return $min;
    }
    return 0;
}

function w2GetCommonNum()
{
    echo "请输入两个数: ";
    fscanf(STDIN, "%d%d", $n1, $n2);

    printf("%d 和 %d的最大公约数是: %d\n", $n1, $n2, getCommonNum($n1, $n2));
}

//球的组合
function w3Ball()
{
    for ($i = 0; $i <= 3; ++$i)
    {
        for ($j = 0; $j <= 3; ++$j)
        {
            printf("红球: %d, 绿球: %d, 黄球: %d\n", $i, $j, 6 - $i - $j);
        }
    }


}

//十进制转其它转制输出函数
function outNum(int $num, int $bit = 16)
{
    if ($num == 0)
        return;

    outNum((int)($num / $bit), $bit);
    echo "0123456789ABCDEF"[$num % $bit];
}

function w4OutBit()
{
    echo "请输入一个数字: ";
    fscanf(STDIN, "%d", $num);
    outNum($num);
    printf("\n");
}

//判断某个数是否为完数
function isPrefectNum(int $num) : bool
{
    for ($i = (int)($num / 2), $s = 0; $i > 0; --$i)
    {
        if ($num % $i == 0)
            $s += $i;
    }

    if ($s == $num)
        return TRUE;

    return FALSE;
}

function w5PrefectNum()
{
    for ($i = 2; $i < 1000; ++$i)
    {
        if (isPrefectNum($i))
            echo $i, " ";
    }
    echo "\n";
}

//4 5 6 7组合数
function w6Num()
{
    $s = 4 + 5 + 6 + 7;
    for ($i = 4; $i <= 7; ++$i)
    {
        for ($j = 4; $j <= 7; ++$j)
        {
            if ($i == $j)
                continue;
            for ($k = 4; $k <= 7; ++$k)
            {
                if ($i == $k || $j == $k)
                    continue;
                $l = $s - $i - $j - $k;
                $n = $i * 1000 + $j * 100 + $k * 10 + $l;
                if ($n % 4 != 0)
                    printf("%d ", $n);
            }
        }
    }
    printf("\n");

    for ($i = 4567; $i <= 7654; $i++)
    {
        $j = $i / 1000 % 10;
        $k = $i / 100 % 10;
        $l = $i / 10 % 10;
        $n = $i / 1 % 10;

        if ($j >= 4 && $j <= 7 && $k >= 4 && $k <= 7
            && $l >= 4 && $l <= 7 && $n >= 4 && $n <= 7
            && $j != $k && $j != $l && $j != $n && $k != $l
            && $k != $n && $l != $n && $i % 4 != 0)
            printf("%d ", $i);
    }
    printf("\n");
}

function isPalindromic(int $num) : bool
{
    if ($num < 10)
        return FALSE;

    for ($n = $num, $sum = 0; $n > 0; $n = (int)($n / 10))
    {
        $sum = $sum * 10 + $n % 10;
    }

    if ($num == $sum)
        return TRUE;

    return FALSE;
}


// $str = "1234ABCD";
// echo $str[5], "\n";

// getIsNum("素数", "isPrime");
// w2GetCommonNum();
// W3Ball();
// w4OutBit();

// w5PrefectNum();

// w6Num();

getIsNum("回文数", "isPalindromic");


