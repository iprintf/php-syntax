#!/usr/bin/php
<?PHP

// function get_num()
function get_num() : int
{
    return 100;
}

function & kyo(int & $val)
{
    echo "isset val = ", isset($val), PHP_EOL;

    $val = 1999;

    return $val;
}

$num = 2000;

$a = & kyo($num);
echo "num = ", $num, PHP_EOL;
echo "a = ", $a, PHP_EOL;
$a = 77777;
echo "num = ", $num, PHP_EOL;

echo get_num(), PHP_EOL;

