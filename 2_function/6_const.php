#!/usr/bin/php
<?PHP

define("MONTH", array(1, 2, 3, 4, 5, 6));


function test()
{
    define("MIN", -1);
    echo "test MAX = ", MAX, PHP_EOL;
    echo "function name = ", __FUNCTION__, PHP_EOL;
    echo "LINE = ", __LINE__, PHP_EOL;
    echo "FILE = ", __FILE__, PHP_EOL;
    echo "DIR = ", __DIR__, PHP_EOL;
}

const YEAR = 1900;
// const YEAR = array(1, 2, 3);

define("MAX", 100);
/*
 * define("MAX", 35.6778);
 * define("MAX", "hello");
 * define("MAX", 'hello');
 * define("MAX", null);
 * define("MAX", TRUE);
 */

// MAX = "6787";
// unset(MAX);
//

test();

echo "MAX = ", MAX, PHP_EOL;
echo "MIN = ", MIN, PHP_EOL;

var_dump(MONTH);

echo "YEAR = ", YEAR, PHP_EOL;

echo "defined(YEAR) = ", defined("YEAR"), PHP_EOL;

// $v = "YEAR";
$v = "MONTH";
echo "constant year = ", constant($v), PHP_EOL;

var_dump(get_defined_constants());



