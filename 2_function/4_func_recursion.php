#!/usr/bin/php
<?PHP

function test(int $num)
{
    if ($num == 0)
        return;

    echo "num = ", $num, PHP_EOL;
    test($num - 1);
    echo "num = ", $num, PHP_EOL;
}
/*
 * test 1 num = 10         10
 * test 2 num = 9          9
 * ...
 * test 10 num = 1         1
 * test 11 num = 0
 */

//阶加 num = 5
// getnum 5   5 + ?  5 + 10
// getnum 4   4 + ?  4 + 6
// getnum 3   3 + ?  3 + 3
// getnum 2   2 + ?  2 + 1
// getnum 1   1 + ?  1 + 0
// getnum 0   0
//
function getNum(int $num)
{
    if ($num == 0)
        return 0;
    return $num + getNum($num - 1);
}

//斐波那契数列  1 1 2 3 5 8 13 21 34 55 89 ...
//6   ? ?       5 + 3
//5   ? ?       3 + 2
//4   ? ?       2 + 1
//3   ? ?       1  + 1
//2   1
function getFb(int $num)
{
    if ($num <= 2)
        return 1;

    return getFb($num - 1) + getFb($num - 2);
}


test(10);
echo "getNum 5 = ", getNum(5), PHP_EOL;
echo "getFb 6 = ", getFb(6), PHP_EOL;

