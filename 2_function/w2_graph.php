#!/usr/bin/php
<?PHP

const BORDER = 5;
const TOP = 3;
const PROMPT= "\t请输入边长/梯高";
const L_UP = 1;
const L_DOWN = 2;
const R_UP = 3;
const R_DOWN = 4;

function showGraphMenu($border, $top)
{
    // var_dump($argv);
    echo "=== 图形打印程序主菜单  ===", PHP_EOL;
    echo "=== 1. 梯形             ===", PHP_EOL;
    echo "=== 2. 正方形           ===", PHP_EOL;
    echo "=== 3. 直角三角形       ===", PHP_EOL;
    echo "=== 4. 设置边长/梯高($border) ===", PHP_EOL;
    echo "=== 5. 设置梯顶($top)      ===", PHP_EOL;
    echo "=== 6. 退出             ===", PHP_EOL;
    echo "请输入[1 - 6]: ";
}

function getMenuInput(int $start, int $end, $show, ...$show_arg): int
{
    //起始值大于结束值就交换
    if ($start > $end)
    {
        $start ^= $end;
        $end ^= $start;
        $start ^= $end;
    }
    // var_dump($show_arg);
    while (TRUE) {
        if ($show == null)
            echo "请输入[$start - $end]: ";
        else
            $show(...$show_arg);
        if (fscanf(STDIN, "%d", $n) < 1 || $n < $start || $n > $end)
            continue;
        break;
    }

    return $n;
}

function showTrapezoid($top = TOP, $height = BORDER)
{
    /*
    3333***3333
    333*****333
    33*******33
    3*********3
    ***********
    */

    $h=$height;
    $w=$top + ($height - 1) * 2;
    $m = $h * $w;

    for ($i = 0; $i < $m; ++$i) {
        $r = $i / $w;
        $c = $i % $w;

        $s = $height - $r - 1;
        if ($c >= $s && $c < $s + $top + $r * 2 - 1)
            echo " * ";
        else
            echo "   ";

        if ($c == $w - 1)
            echo "\n";
    }


    for ($i = 0; $i < $height; ++$i) {
        for ($j = 0, $num = $height - $i - 1; $j < $num; ++$j) {
            echo "   ";
        }
        for ($j = 0, $num = $top + $i * 2; $j < $num; ++$j) {
            echo " * ";
        }
        echo "\n";
    }
}

function showSquare($border = BORDER)
{
    for ($i = 0; $i < $border; ++$i) {
        for ($j = 0; $j < $border; ++$j) {
            echo " * ";
        }
        echo "\n";
    }
}

function setData($start = 3, $end = 20, $prompt = PROMPT)
{
    return getMenuInput($start, $end, function() use ($start, $end, $prompt) {
        echo "${prompt}[$start - $end]: ";
    });
}

function setBorder($prompt = PROMPT)
{
    return setData(3, 20, $prompt);
}

function showTriangleSubMenu($border)
{
    echo "\t--- 直角三角形子菜单    ---", PHP_EOL;
    echo "\t--- 1. 左上边           ---", PHP_EOL;
    echo "\t--- 2. 左下边           ---", PHP_EOL;
    echo "\t--- 3. 右下边           ---", PHP_EOL;
    echo "\t--- 4. 右上边           ---", PHP_EOL;
    echo "\t--- 5. 设置边长 ($border)     ---", PHP_EOL;
    echo "\t--- 6. 返回上一级       ---", PHP_EOL;
    echo "\t--- 7. 退出             ---", PHP_EOL;
    echo "\t请输入[1 - 7]: ";
}

function showTriangleLeftUp($border = BORDER)
{
    for ($i = 0, $m = $border * $border; $i < $m; ++$i) {
        $r = intdiv($i, $border);
        $c = $i % $border;

        if ($c <= $border - $r)
            echo " * ";
        else
            echo "   ";

        if ($c == $border - 1)
            echo "\n";
    }

}

function showTriangleLeftDown($border = BORDER)
{
    for ($i = 0, $m = $border * $border; $i < $m; ++$i) {
        $r = intdiv($i, $border);
        $c = $i % $border;

        if ($c <= $r)
            echo " * ";
        else
            echo "   ";

        if ($c == $border - 1)
            echo "\n";
    }
}

function showTriangleRightUp($border = BORDER)
{
    for ($i = 0, $m = $border * $border; $i < $m; ++$i) {
        $r = intdiv($i, $border);
        $c = $i % $border;

        if ($c >= $r)
            echo " * ";
        else
            echo "   ";

        if ($c == $border - 1)
            echo "\n";
    }
}

function showTriangleRightDown($border = BORDER)
{
    for ($i = 0, $m = $border * $border; $i < $m; ++$i) {
        $r = intdiv($i, $border);
        $c = $i % $border;

        if ($c >= $border - $r - 1)
            echo " * ";
        else
            echo "   ";

        if ($c == $border - 1)
            echo "\n";
    }
}

function _showTriangle($border, $dir = L_UP)
{
    for ($i = 0, $m = $border * $border; $i < $m; ++$i) {
        $r = intdiv($i, $border);
        $c = $i % $border;

        if ($dir == L_UP && $c < $border - $r
            || $dir == L_DOWN && $c <= $r
            || $dir == R_UP && $c >= $r
            || $dir == R_DOWN && $c >= $border - $r - 1)
            echo " * ";
        else
            echo "   ";

        if ($c == $border - 1)
            echo "\n";
    }
}

function showTriangle(int & $border = BORDER): bool
{
    $main_quit = TRUE;
    $quit = TRUE;

    while ($quit)
    {
        $n = getMenuInput(1, 7, "showTriangleSubMenu", $border);


        switch ($n)
        {
            /*
             * case 1:
             *     showTriangleLeftUp($border);
             *     break;
             * case 2:
             *     showTriangleLeftDown($border);
             *     break;
             * case 3:
             *     showTriangleRightUp($border);
             *     break;
             * case 4:
             *     showTriangleRightDown($border);
             *     break;
             */
            case 1:
            case 2:
            case 3:
            case 4:
                _showTriangle($border, $n);
                break;
            case BORDER:
                $border = setBorder("\t\t请输入边长");
                break;
            case 7:
                $main_quit = FALSE;
            case 6:
                $quit = FALSE;
                break;
            default:
                break;
        }
    }

    return $main_quit;
}

function graphInit()
{
    $border = BORDER;
    $top = 3;
    $quit = TRUE;

    while ($quit)
    {
        $n = getMenuInput(1, 6, "showGraphMenu", $border, $top);

        switch ($n) {
            case 1:
                // echo "梯形!\n";
                showTrapezoid($top, $border);
                break;
            case 2:
                // echo "正方形!\n";
                showSquare($border);
                break;
            case 3:
                // echo "直角三角形!\n";
                $quit = showTriangle($border);
                break;
            case 4:
                $border = setBorder();
                break;
            case BORDER:
                $top = setData(3, 10, "\t请输入梯顶");
                break;
            case 6:
                // echo "退出!\n";
                $quit = FALSE;
                // goto QUIT;
                break;
            default:
                // echo "默认!\n";
               break;
        }
    }
// QUIT:

    return 0;
}

exit(graphInit());

