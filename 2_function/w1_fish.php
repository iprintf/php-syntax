#!/usr/bin/php
<?PHP

function isYear(int $year): bool
{
    /*
     * if ($year % 400 == 0 || $year % 4 == 0 && $year % 100 != 0)
     *     return true;
     * return false;
     */
    return ($year % 400 == 0 || $year % 4 == 0 && $year % 100 != 0)
                ? true : false;
}

function countYearDays(int $startYear, int $endYear): int
{
    $days = 0;

    while ($startYear < $endYear) {
        $days += 365 + isYear($startYear++);
    }

    return $days;
}

function countMonthDays(int $month, $year)
{

    $days = ($month - 1) * 30 + intdiv($month, 2);
    $days += ($month == 9 || $month == 11) ? 1 : 0;

    if ($month > 2) {
        $isY = is_bool($year) ? $year : isYear($year);
        $days -= $isY ? 1 : 2;
        // $days = $days - 2 + (is_bool($year) ? $year : isYear($year));

    }
    /*
     * $month > 2 and
     *         $days = $days - 2 + (is_bool($year) ? $year : isYear($year));
     */

    // echo $month, " = ", $days, PHP_EOL;

    return $days;
}

function myCheckDate($year, $month, $day)
{
/*
 *     if ($year < 1900 || $year > 9999
 *             || $month < 1 || $month > 12
 *             || $day < 1 || $day > 31)
 *         return false;
 *
 *     if (($month == 4 || $month == 6 || $month == 9 || $month == 11)
 *             && $day > 30)
 *         return false;
 *
 *     if ($month == 2 && $day > 28 + isYear($year))
 *         return false;
 */

    if ($year < 1900 || $year > 9999
            || $month < 1 || $month > 12
            || $day < 1 || $day > 31
            || (($month == 4 || $month == 6 || $month == 9 || $month == 11)
                    && $day > 30)
            || ($month == 2 && $day > 28 + isYear($year)))
        return FALSE;

    return TRUE;
}

//黑色星期五
function blackFri(int $year = 2016)
{
    $days = countYearDays(1900, $year) + 13;

    $isY = isYear($year);

    for ($i = 1; $i <= 12; $i++) {
        $d = $days + countMonthDays($i, $isY);
        if ($d % 7 == 5)
            printf("%4d 年 %2d 月 13号是黑色星期五!\n", $year, $i);
    }
}

// blackFri(2015);

function fish($day = 1, $month = 1, $year = 2000)
{
    if ($year < 2000)
        return 0;

    $d = (countYearDays(2000, $year)
            + countYearDays($month, $year) + $day) % 5;

    if ($d >= 0 && $d < 3)
        echo "打鱼!\n";
    else
        echo "晒网!\n";
}


printf("请输入年月日: ");
fscanf(STDIN, "%d%d%d", $year, $month, $day);
myCheckDate($year, $month, $day) and fish($day, $month, $year);
// fscanf(STDIN, "%d", $day);
// fish($day);

