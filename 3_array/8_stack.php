#!/usr/bin/php
<?PHP

const MAX = 10;

function isfull($end)
{
    return ($end == MAX ? TRUE : FALSE);
}

function isempty($end)
{
    return ($end == 0 ? TRUE : FALSE);
}

function push($data, & $stack, & $end)
{
    if (isfull($end))
        return -1;

    $stack[$end++] = $data;

    return 0;
}

function pop(& $stack, & $end)
{
    if (isempty($end))
        return -1;
    return $stack[--$end];
}

function main()
{
    $stack = array();
    $end = 0;
    printf("请输入整数: ");
    fscanf(STDIN, "%d", $n);

    while ($n != 0)
    {
        array_push($stack, $n % 8);
        // push($n % 8, $stack, $end);
        // printf("push n %% 8 =  %d\n", $n % 8);
        $n = (int)($n / 8);
    }

    // while (!isempty($end))
    while (count($stack) != 0)
    {
        echo array_pop($stack);
    }
    echo "\n";

    return 0;
}

exit(main());



