#!/usr/bin/php
<?PHP

$a = array(11, 22, 33, 44, 55, 66, 77);

echo "current = ", current($a), PHP_EOL;
echo "end = ", end($a), PHP_EOL;
echo "pos = ", pos($a), PHP_EOL;
echo "key = ", key($a), PHP_EOL;

echo "prev = ", prev($a), PHP_EOL;
echo "current = ", current($a), PHP_EOL;
echo "reset = ", reset($a), PHP_EOL;

print_r(each($a));

reset($a);
/*
 * do
 *     echo pos($a), " ";
 * while (next($a));
 * echo "\n";
 */
/*
 * while (list($key, $val) = each($a))
 * {
 *     echo $key, "|", $val, " ";
 * }
 * echo "\n";
 */

/*
 * $a = 111;
 * $b = 222;
 * $kyo = "hello";
 * $url = "www";
 * 
 * $arr = compact("a", "b", "kyo", "url");
 */
// print_r($arr);
//
$arr = array(
    "kyo" => "hello",
    "url" => "www",
    "abc" => "ttt"
);

// extract($arr);

// echo $kyo, " ", $url, " ", $abc, "\n";

$a = array(
    array(
        'id' => 335,
        'name' => "kyo",
    ),
    array(
        'id' => 123,
        'name' => "tom",
    ),
    array(
        'id' => 567,
        'name' => "mary",
    )
);

function cmpName($v1, $v2)
{
    // echo '$v1 = ', $v1, ', $v2 = ', $v2, PHP_EOL;
    return $v1['name'] <=> $v2['name'];
}

// usort($a, "cmpName");

// print_r($a);

$a = array("pic15.jpg", "pic1.jpg", "pic20.jpg", "pic3.jpg");

// sort($a);
natsort($a);

print_r($a);

