#!/usr/bin/php
<?PHP

$a = [
      'A' => 11111,
      'B' => 2222
     ];

// unset($a);  //删除数组
unset($a['B']);  //删除数组指定元素

print_r($a);

$a[] = "hello";
$a[] = "world";

// function test(array $a)
function test(array & $a)
{
    echo "test print: \n";
    print_r($a);
    $a['A'] = "9999";
}

test($a);

echo "main print:\n";
print_r($a);

function kyo()
{
    return [123, "hello", 3.356];
}

echo "kyo array print:\n";
print_r(kyo());

echo "kyo array 2 val: ", kyo()[2] ,"\n";

// $num = kyo()[1];
// 或
// list($num, $str, $float) = kyo();
// list($num) = kyo();
// list(,$num) = kyo();
// list(,,$num) = kyo();

echo "num = ", $num, PHP_EOL;


