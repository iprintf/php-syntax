#!/usr/bin/php
<?PHP

function main()
{
    $a = array(10, 3, 9, 7, 4, 10, 8, 10, 9, 7);
    $l = count($a);
    $max = 0;
    $sec = 0;

    for ($i = 0; $i < $l; ++$i) {
        for ($j = 0, $c = 0; $j < $l; ++$j) {
            if ($a[$i] == $a[$j])
                $c++;
        }
        if ($c == 1) {
            // echo $a[$i], "\n";
            if ($a[$i] > $max) {
                $sec = $max;
                $max = $a[$i];
            } else if ($sec < $a[$i])
                $sec = $a[$i];
        }
    }

    printf("sec = %d\n", $sec);

    return 0;
}

exit(main());

