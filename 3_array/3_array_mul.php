#!/usr/bin/php
<?PHP

$a = array(array(1, 2, 3), array("A", "B", "C"));

foreach ($a as $row)
{
    foreach ($row as $val)
    {
        echo "val = ", $val, PHP_EOL;
    }
}

/*
 * $arr = array(
 *     array('*', '*', '*'),
 *     array('*', '*', '*'),
 *     array('*', '*', '*')
 * );
 */

$arr = array();

for ($i = 0; $i < 5; ++$i)
{
    for ($j = 0; $j < 5; ++$j)
    {
        $arr[$i][$j] = $i >= $j ? '*': " ";
    }
}


foreach ($arr as $r)
{
    foreach ($r as $c)
    {
        echo " $c ";
    }
    echo "\n";
}

