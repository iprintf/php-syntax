#!/usr/bin/php
<?PHP


function main()
{
    $m = array();
    $w = array();

    while (TRUE)
    {
        printf("输入姓名/性别(end/-1): ");
        fscanf(STDIN, "%s%d", $name, $sex);

        if ($name == "end" || $sex == -1)
            break;

        if ($sex == 1)
            $m[] = $name;
        else
            $w[] = $name;
    }

    $wc = count($w);
    $mc = count($m);
    $min = $wc > $mc ? $mc : $wc;

    for ($i = 0; $i < 3; ++$i)
    {
        printf("第 %d 轮: ", $i + 1);
        for ($j = 0; $j < $min; ++$j)
        {
            $name = array_shift($m);
            printf("%s <==> ", $name);
            $m[] = $name;
            $name = array_shift($w);
            printf("%s ", $name);
            $w[] = $name;
        }
        printf("\n");
    }

    return 0;
}

exit(main());
