#!/usr/bin/php
<?PHP

/*
 * $a = "hello world";
 * 
 * for ($i = 0, $l = strlen($a); $i < $l; ++$i)
 * {
 *     echo $a[$i], " ";
 *     // echo $a{$i}, " ";
 * }
 * echo "\n";
 */

system("stty -echo -icanon");

while (TRUE)
{
    $ch = ord(fgetc(STDIN));
    if ($ch == 27)
        break;
    else if ($ch == ord('w'))
        echo "\033[1A";
    else if ($ch == ord('s'))
        echo "\033[1B";
    else if ($ch == ord('a'))
        echo "\033[1D";
    else if ($ch == ord('d'))
        echo "\033[1C";
    else
        printf("%c", $ch);
}

system("stty echo icanon");
