#!/usr/bin/php
<?PHP

/*
 * 1. 输入颜色控制 \033[*m
 * 2. 定位控制
 *     相对定位  \033[r;cH
 *     绝对定位  \033[*A/B/C/D
 * 3. 其它屏幕控制
 *      保存光标    \033[s      只保存一个光标
 *      加载光标    \033[u
 *      隐藏光标    \033[?25l
 *      显示光标    \033[?25h
 *      清屏        \033[2J
 *      清行        \033[2K
 *      清光标位置后面所有内容  \033[K
 *
 */

for ($i = 0; $i < 8; ++$i)
{
    printf("\033[%dmhello\033[%dmworld \033[1;%d;%dmkyo\033[0m\n",
        30 + $i, 40 + $i, 37, 40 + $i);
}
echo "\033[s";
echo "\033[1;1HKYO";
echo "\033[10;50HKYO";
echo "\033[1AHELLO";
echo "\033[2BHELLO";
echo "\033[uOOOOOOOO";

echo "\033[?25l";

fgetc(STDIN);

echo "\033[?25h";

echo "\033[1;1H\033[2K";
echo "\033[5;6H\033[K";

fgetc(STDIN);

echo "\033[2J\033[1;1H";


