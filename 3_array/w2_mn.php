#!/usr/bin/php
<?PHP

function main()
{
    $p = array();
    $c = 0;
    printf("请输入m & n: ");
    fscanf(STDIN, "%d%d", $m, $n);

    for ($i = 0; $i < $m; ++$i)
    {
        printf("请输入人名[%d]: ", $i);
        fscanf(STDIN, "%s", $p[$i]);
    }

    while ($m != 0)
    {
        foreach ($p as $key => $name)
        {
            if (++$c == $n)
            {
                echo "$name ";
                unset($p[$key]);
                $c = 0;
                $m--;
            }
        }
    }
    echo "\n";

    return 0;
}

exit(main());

