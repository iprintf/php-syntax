#!/usr/bin/php
<?PHP

const MAX = 10;

/*
 * function isfull($end)
 * {
 *     return ($end == MAX ? TRUE : FALSE);
 * }
 */

function isfull($front, $end)
{
    return (($end + 1) % MAX == $front ? TRUE : FALSE);
}

function isempty($front, $end)
{
    return ($end == $front ? TRUE : FALSE);
}

function en($data, & $stack, $front, & $end)
{
    if (isfull($front, $end))
        return -1;

    $stack[$end++] = $data;

    return 0;
}

function de(& $stack, & $front, $end)
{
    if (isempty($front, $end))
        return -1;
    return $stack[$front++];
}

function main()
{
    $stack = array();
    $end = 0;
    $front = 0;

    echo "rand: ";
    for ($i = 0; $i < 15; ++$i)
    {
        $n = mt_rand(1, 100);
        echo "$n ";
        // en($n, $stack, $front, $end);
        // array_push($stack, $n);
        $stack[] = $n;
    }
    echo "\n";

    echo "de: ";
    // while (!isempty($front, $end))
    while (count($stack) != 0)
    {
        echo array_shift($stack), " ";
        // echo de($stack, $front, $end), " ";
    }
    echo "\n";

    return 0;
}

exit(main());



