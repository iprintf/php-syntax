#!/usr/bin/php
<?PHP

declare(ticks=1);
// register_tick_function("myTickFunc");

function myTickFunc()
{
    static $v = 0;
    echo "tick $v\n";
    $v++;
}

register_shutdown_function(function(){
    echo "Bye1!\n";
});
register_shutdown_function(function(){
    echo "Bye2!\n";
});

for ($i = 0; $i < 10; ++$i)
{
    echo "for $i\n";
    if ($i == 5)
        die(0);
}


