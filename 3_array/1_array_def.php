#!/usr/bin/php
<?PHP

$a = array(123, "hello", 356.788);

echo $a[0], ", ", $a[1], ", ", $a[2], PHP_EOL;

for ($i = 0, $l = sizeof($a); $i < $l; ++$i) {
    echo $a[$i], ", ";
}
echo "\n";

$a[0] = 567;

$b[] = 90.354;

$c = ['100' => "aaaa", 'BB' => "bbbb", 89 => "cccc", "ddddd", null => "tttt"];

// foreach ($c as $key => $val) {
foreach ($c as $key => & $val) {
    echo "key = ", $key, ", val = ", $val, PHP_EOL;
    $val = 689;
    // $c[$key] = 19999;
}

print_r($c);

exit(0);

echo '$c[BB] = ', $c['BB'], PHP_EOL;

$c[""] = "TTTTT";

$c[] = "eeee";

var_dump($c);


