#!/usr/bin/php
<?PHP

// $a = range(3, 100, 5);
$a = array_combine(["a", "b", "c", "e"], [100, 200, 300, 400]);

// echo $a[array_rand($a)], PHP_EOL;

// echo "in_array = ", in_array("a", $a), PHP_EOL;

// print_r(array_pad($a, 6, 0));

$a['a'] = array('id' => 13546, 'name' => "kyo");

// print_r($a);

// exit(0);

array_walk_recursive($a, function(& $val, $key, $arg){
    // echo "$val ";
    if ($val == 13546)
        $val = $arg;
}, "hello");
printf("\n");

// print_r($a);

// print_r(array_chunk($a, 2, true));


$a = [1, 2, 'w' => 3];
$b = ["hello", "w" => "world", "kyo"];

// print_r($a + $b);
// print_r(array_replace($a, $b));
$c = array_merge($a, $b);

// print_r(array_slice($c, 2, 2));
array_splice($c, 2, 2, array(999, 888));

print_r($c);

$s = array_fill(0, 10, "kyo");

print_r($s);

$a = array(1, 2, 3, 4, 1, 2, 2, 1, 2, 1, 1);

print_r(array_count_values($a));



