#!/bin/bash

arg=$(./d_memory)
echo $arg
n1=$(echo $arg | awk '{print $1}')
n2=$(echo $arg | awk '{print $2}')

val=$((n2 - n1))
echo $val;

if [ $val -gt 1024 ]; then
    k=$[val / 1024]
    if [ $k -gt 1024 ];then
        m=$[k / 1024]
        echo ${m}M
    else
        echo ${k}K
    fi
else
    echo ${val}B
fi

