#!/usr/bin/php
<?PHP

function arrayInit(array & $arr, int $max = 0,
                        bool $rand = TRUE, int $rand_max = 9999999)
{
    for ($i = 0, $m = $max == 0 ? sizeof($arr): $max; $i < $m; ++$i) {
        if ($rand)
            $arr[$i] = mt_rand(0, $rand_max);
        else {
            printf("请输入整型数(%d): ", $i);
            fscanf(STDIN, "%d", $arr[$i]);
        }
    }
}

function arrayOut(array & $arr, string $prompt = "")
{
    echo "$prompt";
    foreach ($arr as $val) {
        echo "$val ";
    }
    echo "\n";
}

function arrayMaxMin(array & $arr, bool $flag = TRUE): int
{
    $m = $arr[0];

    foreach ($arr as $val) {
        if ($flag == TRUE && $val > $m
            || $flag == FALSE && $val < $m)
            $m = $val;
    }

    return $m;
}

function arrayRev(array & $a)
{
    for ($i = 0, $c = sizeof($a), $m = (int)($c / 2); $i < $m; ++$i) {
        $a[$i] ^= $a[$c - $i - 1];
        $a[$c - $i - 1] ^= $a[$i];
        $a[$i] ^= $a[$c - $i - 1];
    }
}

function arrayLeft(array & $a, $bit = 1)
{
    for ($j = 0; $j < $bit; $j++) {
        for ($i = 0, $l = sizeof($a) - 1; $i < $l; ++$i) {
            $a[$i] ^= $a[$i + 1];
            $a[$i + 1] ^= $a[$i];
            $a[$i] ^= $a[$i + 1];
        }
    }
}

function arrayRight(array & $a, $bit = 1)
{
    $c = sizeof($a);

    for ($j = 0; $j < $bit; $j++) {
        for ($i = $c - 1; $i > 0; --$i) {
            $a[$i] ^= $a[$i - 1];
            $a[$i - 1] ^= $a[$i];
            $a[$i] ^= $a[$i - 1];
        }
    }
}

$a = array();

// arrayInit($a, 10, FALSE);
arrayInit($a, 10, TRUE, 999);
arrayOut($a, "a = ");
printf("max = %d, min = %d\n", arrayMaxMin($a), arrayMaxMin($a, FALSE));
printf("max = %d, min = %d\n", max($a), min($a));
arrayRev($a);
arrayOut($a, "Rev = ");

arrayLeft($a);
arrayOut($a, "Left = ");
arrayRight($a);
arrayOut($a, "Right = ");

