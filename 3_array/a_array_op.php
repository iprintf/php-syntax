#!/usr/bin/php
<?PHP

$a = array(1, 2, 3);
$b = array("2" => 3, 1 => 2, 0 => 1);
// $b = array(4 => 4, 5 => 5, 6 => 6);
$c = $a + $b;

// if ($a != $b)
if ($a == $b)
    echo "HELLO\n";

var_dump($c);
