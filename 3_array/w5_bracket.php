#!/usr/bin/php
<?PHP

function check_bracket(string $s): int
{
    $stack = array();

    for ($i = 0, $l = strlen($s); $i < $l; ++$i)
    {
        if ($s[$i] == '{' || $s[$i] == '[' || $s[$i] == '(')
            $stack[] = $s[$i];
        else if ($s[$i] == '}' || $s[$i] == ']' || $s[$i] == ')')
        {
            $ch = array_pop($stack);
            if ($ch == NULL)
                return 1;
            if ($ch == '{' && $s[$i] != '}'
                    || $ch == '[' && $s[$i] != ']'
                    || $ch == '(' && $s[$i] != ')')
                return 2;
        }
    }
    if (count($stack) > 0)
        return 3;

    return 0;
}

function main()
{
    printf("请输入一行字符串: ");
    $s = fgets(STDIN);
    $err = ["", "缺少左括号!\n", "左右括号不匹配!\n", "缺少右括号!\n"];

    echo $err[check_bracket($s)];

    return 0;
}

exit(main());

