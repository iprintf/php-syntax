#!/usr/bin/php
<?PHP

function fb($num)
{
    if ($num <= 2)
        return 1;
    return fb($num - 1) + fb($num - 2);
}

function w1_fb()
{
    // 1 1 2 3 5 8 13 21 34 ...
    $num = 8;

    echo fb($num), PHP_EOL;

    $n1 = 1;
    $n2 = 1;
    for ($i = 3; $i <= $num; $i++)
    {
        $n = $n1 + $n2;
        $n1 = $n2;
        $n2 = $n;
    }

    echo $n, PHP_EOL;
}

function select(& $arr)
{
    $c = count($arr);
    $l = $c - 1;

    for ($i = 0; $i < $l; ++$i) {
        $n = $arr[$i];
        $index = $i;
        for ($j = $i + 1; $j < $c; ++$j) {
            if ($n > $arr[$j]) {
                $n = $arr[$j];
                $index = $j;
            }
        }
        $arr[$index] = $arr[$i];
        $arr[$i] = $n;
    }
}

function sum($num)
{
    $n = $num;
    $s = 0;

    while ($n != 0)
    {
        $b = $n % 10;
        $s += $b * $b * $b;
        $n = (int)($n / 10);
    }
    if ($s == $num)
        return true;
    return false;
}

// w1_fb();

// ==================================================

$a = array(5, 1, 10, 8, 65, 3, 15, 99, 53, 12, 2);
select($a);
print_r($a);

// ==================================================

for ($i = 1; $i < 1000; ++$i) {
    if (sum($i))
        printf("%d ", $i);
}
echo "\n";

// ==================================================
$a = [3, 5, 7, 8];
$s = $a[0] + $a[1] + $a[2] + $a[3];
$c = 0;

for ($i = 0; $i < 4; ++$i) {
    for ($j = 0; $j < 4; ++$j) {
        if ($a[$i] == $a[$j])
            continue;
        for ($k = 0; $k < 4; ++$k) {
            if ($a[$i] == $a[$k] || $a[$j] == $a[$k])
                continue;

            $l = $s - $a[$i] - $a[$j] - $a[$k];

            if ($l != 5) {
                $n = $a[$i] * 1000 + $a[$j] * 100 + $a[$k] * 10 + $l;
                printf("%d ", $n);
                $c++;
            }
        }
    }
}
printf("\ncount = %d\n", $c);

