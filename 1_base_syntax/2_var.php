#!/usr/bin/php
<?php

$a = 123;

$a = 456;

// $b = $a;
$b = & $a;

$a = 123;

$name = "kyo";
$$name = "php";

echo 'isset(s) = ', isset($a), PHP_EOL;
$s = false;
echo 'empty(s) = ', empty($s), PHP_EOL;

echo "a = $a, b = $b, name = $name", PHP_EOL;
echo '$kyo = ', $kyo, PHP_EOL;
echo '$$name = ', $$name, PHP_EOL;

// unset($a);

echo "$a", PHP_EOL;
echo "${a}hello", PHP_EOL;
echo "{$a}hello", PHP_EOL;


?>
