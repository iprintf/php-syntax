#!/usr/bin/php
<?PHP

$a = 10;
$b = 20;
echo $a++, PHP_EOL;
echo ++$a, PHP_EOL;
echo $a, PHP_EOL;

echo 'xor = ', (true xor false), PHP_EOL;

$c = $b >= 20 ? $a : $b;

// $c = isset($s) ? $s : false;
// $c = isset($s) ? $s : $b;
$c = $s ?? $t ?? $a;

echo "c = ", $c, PHP_EOL;

/*
 * echo "a = ", $a, ", b = ", $b , PHP_EOL;
 * echo "a + b = ", $a + $b, PHP_EOL;
 * echo "a - b = ", $a - $b, PHP_EOL;
 * echo "a * b = ", $a * $b, PHP_EOL;
 * echo "a / b = ", $a / $b, PHP_EOL;
 * echo "a % b = ", $a % $b, PHP_EOL;
 *
 * echo "a > b = ", $a > $b, PHP_EOL;
 * echo "a >= b = ", $a >= $b, PHP_EOL;
 * echo "a < b = ", $a < $b, PHP_EOL;
 * echo "a <= b = ", $a <= $b, PHP_EOL;
 * echo "a != b = ", $a != $b, PHP_EOL;
 */

/*
 * $a = "10";
 * $b = 10;
 * echo '--------------------------------', PHP_EOL;
 *
 * echo "a == b = ", $a == $b, PHP_EOL;
 * echo "a === b = ", $a === $b, PHP_EOL;
 * echo "a != b = ", $a != $b, PHP_EOL;
 * echo "a !== b = ", $a !== $b, PHP_EOL;
 *
 * echo "a <> b = ", $a <> $b, PHP_EOL;
 * echo "a <=> b = ", $a <=> $b, PHP_EOL;
 *
 */

$a = -5;
$b = -3;
//000...101
//111...010
//111...001
//000...110
echo '~a = ', ~$a, PHP_EOL;

// 5 101 >> 1  = 10
// -5 补码 111...011 >> 1  111...101
//  补码转换: 对补码减1按位取位  111...100  000...011 -3
echo 'a >> 1 = ', $a >> 1, PHP_EOL;
// 5 101 << 1  = 1010
// -5 补码 111...011 << 1  111...0110
//  补码转换: 对补码减1按位取位  111...0101 000...1001
echo 'a << 1 = ', $a << 1, PHP_EOL;

//101
//011
//001  1
echo 'a & b = ', $a & $b, PHP_EOL;
//101
//011
//111  7
echo 'a | b = ', $a | $b, PHP_EOL;
//101
//011
//110  6
echo 'a ^ b = ', $a ^ $b, PHP_EOL;



