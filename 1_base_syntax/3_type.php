#!/usr/bin/php
<?php

echo 'PHP_INT_MAX = ', PHP_INT_MAX, PHP_EOL;

$a = 0b10;

$a = NULL;
// $a = 0b10;
// $a = 235.6778;
// $a = array();
// $a = "123hello";
// $a = TRUE;

$b = (int)$a;
echo "b = ", $b, ", b type = ", gettype($b), PHP_EOL;

echo 'is_bool(a) = ', is_bool($a), PHP_EOL;

echo "a = ", $a, PHP_EOL;
echo "a type = ", gettype($a), PHP_EOL;


